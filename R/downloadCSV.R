#' A simple wrapper to download CSV files
#'@description Performs a search, and downloads CSV files from HIEv. Beware, no error checking or other niceties are included at the moment. Returns a list of dataframes, or a single dataframe if only one match occurred.
#'@param filename The filename to search on HIEv.
#'@param hievSearch Optionally, an object returned by \code{\link{searchHIEv}}.
#'@param startDate (YYYY-MM-DD) can be used to dictate where the dataframe starts.
#'@param endDate (YYYY-MM-DD) can be used to dictate where the dataframe ends.
#'@param datetimevar Name of the POSIXct variable in the CSV file (format has to be YYYY-MM-DD HH:MM(:SS)).
#'@param rowbind If TRUE, combines multiple CSV files by row. If FALSE, returns a list of dataframes.
#'@param addFileName If TRUE, adds filename as column to the resulting dataframe.
#'@param topath Path to download files to, overrides \code{\link{setToPath}}.
#'@param maxnfiles Maximum number of files to download from HIEv.
#'@param quiet If FALSE, prints no messages of any kind.
#'@param tryoffline If no internet connection or no valid API key, tries to find and read files from the target path (set by \code{\link{setToPath}})
#'@param \dots Further arguments passed to \code{\link{searchHIEv}}.
#'@export
downloadCSV <- function(filename=NULL,
                        hievSearch=NULL,
                        startDate=NULL,
                        endDate=NULL,
                        datetimevar="DateTime",
                        rowbind=TRUE,
                        addFileName=TRUE,
                        topath=NULL,
                        maxnfiles=50,
                        quiet=FALSE,
                        tryoffline=FALSE,
                        ...){
  
  
  if(is.null(topath) & "hievtopath" %in% names(options())){
    topath <- options()$hievtopath
  } else {
    topath <- getToPath()
  }
  
  if (!is.null(hievSearch)){
    resultAPI <- hievSearch
  } else {
    resultAPI <- searchHIEv(filename=filename, startDate=startDate, 
                            endDate=endDate, quiet=quiet, tryoffline=tryoffline, ...) 
  }
  
  dfn <- downloadHIEv(resultAPI, topath=topath, maxnfiles=maxnfiles,
                      quiet=quiet)
  dfn <- file.path(topath,dfn)
  nfiles <- nrow(resultAPI)
    
  readCSV <- function(filename, ...){
    dfr <- try(as.data.frame(fread(filename, ...)))
    if(inherits(dfr, "try-error"))
      stop("Unable to read downloaded file as CSV, are you sure it is a CSV file?")
    if(addFileName)dfr$Source <- basename(filename)
    return(dfr)
  }
  
  if(!quiet)message("Reading files...", appendLF=FALSE)
  dats <- lapply(dfn, readCSV)
  if(!quiet)message("done.")
  
  if(length(dats)==1)
    dats <- dats[[1]]
  
  if(rowbind){
    
    if(nfiles > 1){
      message("Combining dataframes...", appendLF=FALSE)
      dats <- suppressWarnings(as.data.frame(rbindlist(dats, fill=TRUE)))
      message("done.")
    }
    
    if(datetimevar %in% names(dats)){

      dats <- as.data.frame(dats) # avoid dplyr bug!
      
      tryconv <- try(as.POSIXct(as.character(dats[,datetimevar]),tz="UTC"))
      
      if(!inherits(tryconv,"try-error")){
        dats[,datetimevar] <- tryconv
        
        if(!is.null(endDate))dats <- dats[as.Date(dats[,datetimevar]) <= as.Date(endDate),]
        if(!is.null(startDate))dats <- dats[as.Date(dats[,datetimevar]) >= as.Date(startDate),]
      } else {
        if(!quiet)warning("DateTime conversion failed, returning data as is.")
        
      }
    }
    
  }
  
  
return(dats)
}


