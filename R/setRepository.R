#' Set the URL of the HIEv repository
#' @description Set or get the URL of the repository. Used by \code{\link{searchHIEv}}.
#' @param The URL of the repository, including the API search keyword. Defaults to HIEv at UWS.
#' @rdname getRepository
#' @export
setRepository <- function(repos="https://hiev.uws.edu.au", ...){
  
  assign("hievrepos",repos, envir = HIEv:::.hievenv)
  
}

#' @rdname getRepository
#' @export
getRepository <- function(...){
  
  l <- ls(envir=HIEv:::.hievenv)
  if(!"hievrepos" %in% l)
    return(NULL)
  else
    return(get("hievrepos", envir = HIEv:::.hievenv))
}


