#' Sets the authentication token for the current session
#' 
#' @description A simple utility to set the authentication token needed to access the HIEv API. This
#' needs to be done once per session. If the token is not set, \code{\link{searchHIEv}} will complain and exit.
#' 
#' The function \code{getToken} finds the token, if it is not yet set it will return \code{NULL}.
#' @details To find your API token, login to the HIEv web portal, click on your email address (topright), and select Settings. Your API token is displayed there.
#' @param token Optional : provide the token as an argument or paste it when prompted.
#' @param tokenfile A text file with the token (as one line, without quotation marks). See details.
#' @param interactive Logical, if FALSE, setToken was invoked by another function (for programming purposes only).
#' @param quiet If TRUE, does not print any messages to the screen.
#' @return Invisibly returns whether the token was set successfully.
#' @details If the tokenfile exists, \code{setToken} does not have to be run by the user, as \code{searchHIEv} will attempt to set the token (through a call to \code{setToken}).
#' @export
#' @rdname setToken
setToken <- function(token=NULL, tokenfile="c:/hiev/token.txt", interactive=TRUE, quiet=!interactive){
  
  if(is.null(token)){
    if(!file.exists(tokenfile) && interactive)
      token <- readline("Paste your token here: ")
    
    if(file.exists(tokenfile)){
      token <- readLines(tokenfile, warn=FALSE)[1]
      if(!quiet)message("Token read from ",tokenfile)
    }
    
    if(!file.exists(tokenfile) && !interactive)
      return(invisible(FALSE))
  }
  
  if(nchar(token) < 15)
    stop("Token is malformed. Find the token from HIEv!")
  
  token <- gsub('"', '', token)
  
  # trim white space (might happen when copy-pasting). Reported by Chelsea.
  token <- str_trim(token)
  
  # Assign global (hidden) variable.
  assign("hievtoken", token, envir = HIEv:::.hievenv)

return(invisible(TRUE))
}

#'@rdname setToken
#'@export
getToken <- function(){
  
  l <- ls(envir=HIEv:::.hievenv)
  if(!"hievtoken" %in% l)
    return(NULL)
  else
    return(get("hievtoken", envir=HIEv:::.hievenv))
  
}




