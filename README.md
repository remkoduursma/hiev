# HIEv

An R package to interface with the data management system HIEv at the [Hawkesbury Institute for the Environment](http://www.westernsydney.edu.au/hie).

To use the HIEv R package, you must already have an active HIEv account. Contact Gerry Devine for questions about HIEv in general (g.devine@westernsydney.edu.au)

Brief instructions are provided below. For a more comprehensive walkthrough, [continue reading here](http://rpubs.com/remkoduursma/144788).


## Installation instructions

To install the package, use the `devtools` package as follows. Windows users must have Rtools installed (http://cran.r-project.org/bin/windows/Rtools/)

```
library(devtools)
install_bitbucket("remkoduursma/HIEv")
library(HIEv)
```

Next, you must set up the API token. The token can be stored in a file called `c:/hiev/token.txt`, or set in the R session (you only have to do this once per R session, the token is stored in a global variable)

```
setToken("<MY_API_TOKEN>")
```

### Searching HIEv

To perform a search, take a look at `?searchHIEv`.

For example,

```
ring3 <- searchHIEv(filename="FACE_R3")
```

This will find all files that contain 'FACE_R3'. Regular expressions are allowed here as well (most of them work anyway). The object `ring3` is a dataframe containing lots of information returned by the HIEv API.

You can also search for filenames that contain FACE and PAR and 2014, by supplying a vector:
```
facepar2014 <- searchHIEv(filename=c("FACE","PAR","2014"))
```
This will find filenames that contain those terms (in any order). See `?searchHIEv` for more options (especially `exclude` to exclude filenames that contain certain keywords).

Now we can download one or many files from the above search result, which we called ring3.

```
downloadHIEv(ring3, which=c(1,5,10))
```

Will download the 1st, 5th and 10th record found in the search.

Finally, the `downloadTOA5` function is for convenience. It can find TOA5-style data, given a search query which includes start and end dates, and coerces all resulting files into a dataframe.
So, for example,

```
dat <- downloadTOA5(filename="ROS_3_Table2", startDate="2013-5-1") 
```

will download data from ROS plot 3, and only keep data after the 1st of May 2013. In this case, it combines three files automatically.