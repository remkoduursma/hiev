

.token <- "qXSmQcAdiebr1NwDBVbA"
URL_Upload <- "http://w0297.uws.edu.au/data_files/api_create.json?"
auth_token <- paste("auth_token=",.token,sep="")



df <- data.frame(xvar=runif(100), yvar=rnorm(100))

fn <- "testfile.csv"
write.csv(df, paste0("R_other/",fn), row.names=FALSE)

file <- "testfile.csv"
type <- "RAW"
# 39 is my LAI project
experiment_id <- "39"

add <- function(arg, cmd){
  if(!is.null(get(arg)))
    paste0(cmd, "&", arg, "=", get(arg))
  else
    cmd
}

description <- "Flat canopy photos following the protocol of MacFarlane et al 2007 (AgForMet143) at 21-22 locations per ring. The included file Filekey_YYYY_MM_DD.csv lists the Northing and Easting for each photo inside the ring."

cmd <- paste0(URL_Upload,auth_token)
cmd <- add("file",cmd)
cmd <- add("type",cmd)
cmd <- add("experiment_id",cmd)
# cmd <- add("description",cmd)

cmd <- URLencode(cmd)

http_result <- httpPOST(cmd)
hr <- fromJSON(http_result)


