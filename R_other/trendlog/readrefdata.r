#read in data from Alterton Envision trendlogs calc minutley means and store in database.

library(RODBC)


readtrendlog<-function(filename){
      
	
	  #read data from trendlog MDB file and average by minute
	  con <- odbcConnectAccess(filename)
	  tblname<-paste(c("tbl",substr(filename,1,27)),collapse="")
      selected<-sqlQuery(con,paste("select  * from",tblname),as.is=T)
	  close(con)
	  #browser()
	  selected<-selected[selected$ValueType==2,]
	  selected$datetime<-as.POSIXct(selected$TimeOfSample,tz="GMT")
	  selected$minute<- format(selected$datetime, '%Y-%m-%d %H:%M')
	  meanval<-aggregate(selected[,"SampleValue"], by=list(selected$minute), FUN=mean,na.rm=TRUE)
	 
	  names(meanval)<-c("datetime","value")
	  meanval$datetime<-as.POSIXct(meanval$datetime,tz="GMT")
	  
	  return(meanval)

}


 rootname<-"F:/TreeChambers_Backup/Memeo/TreeChambers_Backup/C_/Users/UWS/Desktop/Trendlog/Data/Trendlog_0008000_0000000"

# Working directory and root name of the location of the Data.
#workingdir <- "C:/remko/SYDNEY/Data Management/HFE Part2/read trend log"
#rootname <- paste(workingdir, "/Data/Trendlog_0008000_0000000", sep="")
workingdir<-"C:/craig/WTC3/database/trendlogs"
setwd(workingdir)
#load list of trendlogs to be updated
# trendlogs <- read.csv(file="U:/Research Proposals/WTC2/trendlogdata/trendloglist.csv",head=TRUE,sep=",",as.is=T)
trendlogs <- read.csv(file="trendloglist.csv",header=TRUE,as.is=TRUE)

# Ignore data before this date
startdate<-as.Date("2013-05-13")#inclusive
enddate<-as.Date("2013-05-19")#inclusive
# more options..
keepall <- FALSE   # if TRUE, deletes duplicates

# Setup a master list with results: this is a list with 13 elements.
# The first 12 are the chambers, 13 is reference.
master_list <- vector("list", 13)
names(master_list) <- c("ch01","ch02","ch03","ch04","ch05","ch06",
                        "ch07","ch08","ch09","ch10","ch11","ch12","ref")



# Select which variables we want to read (row numbers in 'trendlogs' dataframe).


# 91,92,3,203						

# Loop through those.
for(trendlogs_item in 74:78){

	s1 <- Sys.time()
	
	varname<-trendlogs[[7]][trendlogs_item]
	chamber<-trendlogs[[6]][trendlogs_item]
	pathname<-paste(c(rootname,trendlogs[[3]][trendlogs_item],"/"),collapse="")

	# Try to go to that path - prints an error if failed.
	pathset <- try(setwd(pathname))
	if(inherits(pathset, "try-error"))warning("Could not set path to ",pathname)

	#make list of files where date is greater than startdate
	lf <- list.files()
	m <- grep("-M-",lf,fixed=TRUE)
	if(length(m)>0)lf <- lf[-m]
	m <- grep("@",lf,fixed=TRUE)
	if(length(m)>0)lf <- lf[-m]
	m <- grep("ldb",lf,fixed=TRUE)
	if(length(m)>0)lf <- lf[-m]
	
	datestr <- try(as.Date(substr(lf,31,40)))
	# Error checking : prints warning if dates are malformed.
	if(inherits(datestr, "try-error"))warning("Date string in wrong place in some file(s)")
	if(any(is.na(datestr))){
		warning("Date in wrong place in file ", lf[which(is.na(datestr))])
		next
	}
	lf2 <- subset(lf,datestr>=startdate & datestr<=enddate)
	
	# Read data:
	logs <- lapply(lf2, readtrendlog)

	logdata <- do.call("rbind", logs)
	if(!keepall)logdata <- logdata[!duplicated(logdata$datetime),]
	
	logdata <- logdata[order(logdata$datetime),]
    logdata$chamber<-chamber
	names(logdata)[2] <- varname
	
	# Dump data in the master list.
	master_list[[chamber]] <- c(master_list[[chamber]], list(logdata))

	s2 <- Sys.time()
	cat("Done reading trendlog number",trendlogs_item,"in",round(as.numeric(s2-s1),2),"sec.\n")
	flush.console()
}
closeAllConnections()

#generate master data frame
masterDF<-merge(master_list$ref[[1]],master_list$ref[[2]],by=c("datetime","chamber"),all=T)
for (i in 3:5){
masterDF<-merge(masterDF,master_list$ref[[i]],by=c("datetime","chamber"),all=T)}
#str(masterDF)
cat(paste("from",min(masterDF$datetime)," to",max(masterDF$datetime),sep=" " ))
cat(paste(nrow(masterDF),"minutely data points or",nrow(masterDF)/(60*24)," days worth"))


#add data to database
con<-odbcConnectAccess2007("c:/craig/WTC3/database/trendlogs/trendlogs3.accdb")
sqlSave(con,masterDF,tablename="RefAlertonMin",append=T,rownames=F)#,fast=F,verbose=T)
close(con)