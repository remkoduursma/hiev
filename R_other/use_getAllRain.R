

library(HIEv)
setToPath("c:/hievcache")
source("R_other/getAllRain.R")

#
#rain <- getAllRain(startDate="2015-1-1")

# simple plot 
plot(rain)

# note that rain is a list of dataframes, since the DateTimes may differ between gauges
names(rain)

# So merge before 1:1 plotting:
m <- merge(rain$FACE_R1, rain$FACE_R3, by="DateTime", all=FALSE)
with(m, plot(rain.x, rain.y))   # rain.x is then FACE_R1, rain.y FACE_R3


windows(6,5)
par(mar=c(3,5,2,2), cex.lab=1.2)
plot(getAllRain("2016-6-1"), lwd=2, which=1:5)
dev.copy2pdf(file="rain_june_2016.pdf")


